import { Module, OnApplicationBootstrap } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { UsersModule } from "./users/users.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule } from "./auth/auth.module";
import { IntegratorsModule } from "./integrators/integrators.module";
import { SucursalsIntegratorsModule } from "./sucursals_integrators/sucursals_integrators.module";
import { SeederService } from "./seeders/seeder.service";
import { User } from "./users/entities/user.entity";
import { CountriesModule } from './countries/countries.module';
import { BanksCountriesModule } from './banks-countries/banks-countries.module';
import { ClientsModule } from './clients/clients.module';
import { RatesModule } from './rates/rates.module';
import { FundsModule } from './funds/funds.module';
import { TransactionsModule } from './transactions/transactions.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forRoot({
      type: "mysql",
      host: "127.0.0.1",
      port: 3306,
      username: "root",
      password: "root",
      database: "dolar_daily",
      autoLoadEntities: true,
      synchronize: true,
    }),
    AuthModule,
    UsersModule,
    IntegratorsModule,
    SucursalsIntegratorsModule,
    CountriesModule,
    BanksCountriesModule,
    ClientsModule,
    RatesModule,
    FundsModule,
    TransactionsModule,
  ],
  controllers: [AppController],
  providers: [AppService, SeederService],
})
export class AppModule implements OnApplicationBootstrap {
  constructor(private readonly seederService: SeederService) {}

  async onApplicationBootstrap() {
    await this.seederService.seed();
  }
}
