import { ApiProperty } from "@nestjs/swagger";
import { IsNumber, IsNotEmpty } from "class-validator";

export class CreateRateDto {
    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    monto: number

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    country: number
}
