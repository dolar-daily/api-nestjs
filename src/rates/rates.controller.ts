import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { RatesService } from './rates.service';
import { CreateRateDto } from './dto/create-rate.dto';
import { UpdateRateDto } from './dto/update-rate.dto';
import { ApiBearerAuth, ApiTags, ApiBasicAuth } from '@nestjs/swagger';
import { AuthGuard } from 'src/auth/auth.guard';
import { Roles } from 'src/decorators/roles.decorator';
import { RolesGuard } from 'src/auth/roles/roles.guard';

@ApiBearerAuth()
@ApiTags("Tasas")
@UseGuards(AuthGuard, RolesGuard)
@ApiBasicAuth("Bearer")
@Controller('rates')
export class RatesController {
  constructor(private readonly ratesService: RatesService) {}

  @Roles(['SADMIN'])
  @Post()
  create(@Body() createRateDto: CreateRateDto) {
    return this.ratesService.create(createRateDto);
  }

  @Roles(['SADMIN', 'SINTEGRADOR'])
  @Get()
  findAll() {
    return this.ratesService.findAll();
  }

  @Roles(['SADMIN'])
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ratesService.findOne(+id);
  }

  @Roles(['SADMIN'])
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRateDto: UpdateRateDto) {
    return this.ratesService.update(+id, updateRateDto);
  }

  @Roles(['SADMIN'])
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ratesService.remove(+id);
  }
}
