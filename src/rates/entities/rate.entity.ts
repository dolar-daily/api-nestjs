import { ApiProperty } from "@nestjs/swagger";
import { Country } from "src/countries/entities/country.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Rate {
    @ApiProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "decimal", precision: 10, scale: 2, default: 0.00 })
    monto: number;

    @Column({ default: true })
    status: boolean;
    
    @ManyToOne(() => Country)
    country: Country;

    @ApiProperty()
    @DeleteDateColumn()
    deletedAt: Date;

    @ApiProperty()
    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @ApiProperty()
    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
}
