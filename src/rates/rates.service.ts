import { Injectable, Res } from '@nestjs/common';
import { CreateRateDto } from './dto/create-rate.dto';
import { UpdateRateDto } from './dto/update-rate.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Rate } from './entities/rate.entity';
import { Repository } from 'typeorm';
import { Country } from 'src/countries/entities/country.entity';
import { ResponseUtil } from 'src/utils/response.util';

@Injectable()
export class RatesService {

  constructor(
    @InjectRepository(Rate)
    private readonly rateRepository: Repository<Rate>,
    @InjectRepository(Country)
    private readonly countryRepository: Repository<Country>
  ){}

  async create(createRateDto: CreateRateDto) {
    const validate = await this.countryRepository.findOneBy({id: createRateDto.country})

    if(validate === null)
    {
      return ResponseUtil.error("El pais indicado no existe")
    }

    const validate2 = await this.rateRepository.createQueryBuilder("rate")
    .innerJoin("rate.country", "country")
    .where("country.id = :id", { id: validate.id })
    .getOne();

    if(validate2 !== null)
    {
      return ResponseUtil.error("Ya este pais tiene una tasa")
    }

    const data = await this.rateRepository.save({monto: createRateDto.monto, country: validate})
    return ResponseUtil.success(data, "Registro Realizado con exito")
  }

  async findAll() {
    const data = await this.rateRepository.find({ relations: ["country"] });
    return ResponseUtil.success(data, "Listado obtenido con exito")
  }


  findOne(id: number) {
    return `This action returns a #${id} rate`;
  }

  update(id: number, updateRateDto: UpdateRateDto) {
    return `This action updates a #${id} rate`;
  }

  remove(id: number) {
    return `This action removes a #${id} rate`;
  }
}
