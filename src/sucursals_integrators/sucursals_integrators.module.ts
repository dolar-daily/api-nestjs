import { Module } from '@nestjs/common';
import { SucursalsIntegratorsService } from './sucursals_integrators.service';
import { SucursalsIntegratorsController } from './sucursals_integrators.controller';

@Module({
  controllers: [SucursalsIntegratorsController],
  providers: [SucursalsIntegratorsService],
})
export class SucursalsIntegratorsModule {}
