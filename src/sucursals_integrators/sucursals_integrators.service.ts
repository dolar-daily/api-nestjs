import { Injectable } from '@nestjs/common';
import { CreateSucursalsIntegratorDto } from './dto/create-sucursals_integrator.dto';
import { UpdateSucursalsIntegratorDto } from './dto/update-sucursals_integrator.dto';

@Injectable()
export class SucursalsIntegratorsService {
  create(createSucursalsIntegratorDto: CreateSucursalsIntegratorDto) {
    return 'This action adds a new sucursalsIntegrator';
  }

  findAll() {
    return `This action returns all sucursalsIntegrators`;
  }

  findOne(id: number) {
    return `This action returns a #${id} sucursalsIntegrator`;
  }

  update(id: number, updateSucursalsIntegratorDto: UpdateSucursalsIntegratorDto) {
    return `This action updates a #${id} sucursalsIntegrator`;
  }

  remove(id: number) {
    return `This action removes a #${id} sucursalsIntegrator`;
  }
}
