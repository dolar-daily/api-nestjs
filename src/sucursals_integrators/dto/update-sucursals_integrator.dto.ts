import { PartialType } from '@nestjs/swagger';
import { CreateSucursalsIntegratorDto } from './create-sucursals_integrator.dto';

export class UpdateSucursalsIntegratorDto extends PartialType(CreateSucursalsIntegratorDto) {}
