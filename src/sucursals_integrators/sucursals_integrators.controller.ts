import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { SucursalsIntegratorsService } from './sucursals_integrators.service';
import { CreateSucursalsIntegratorDto } from './dto/create-sucursals_integrator.dto';
import { UpdateSucursalsIntegratorDto } from './dto/update-sucursals_integrator.dto';
import { ApiBasicAuth, ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from 'src/auth/auth.guard';
import { RolesGuard } from 'src/auth/roles/roles.guard';
import { Roles } from 'src/decorators/roles.decorator';


@ApiTags("Sucursales por Integrador")
@ApiBearerAuth()
@UseGuards(AuthGuard, RolesGuard)
@ApiBasicAuth("Bearer")
@Controller('sucursals-integrators')
export class SucursalsIntegratorsController {
  constructor(private readonly sucursalsIntegratorsService: SucursalsIntegratorsService) {}

  @Roles(['SADMIN'])
  @Post()
  create(@Body() createSucursalsIntegratorDto: CreateSucursalsIntegratorDto) {
    return this.sucursalsIntegratorsService.create(createSucursalsIntegratorDto);
  }

  @Roles(['SADMIN'])
  @Get()
  findAll() {
    return this.sucursalsIntegratorsService.findAll();
  }

  @Roles(['SADMIN'])
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.sucursalsIntegratorsService.findOne(+id);
  }

  @Roles(['SADMIN'])
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSucursalsIntegratorDto: UpdateSucursalsIntegratorDto) {
    return this.sucursalsIntegratorsService.update(+id, updateSucursalsIntegratorDto);
  }

  @Roles(['SADMIN'])
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.sucursalsIntegratorsService.remove(+id);
  }
}
