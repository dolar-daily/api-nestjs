export enum TransactionType {
    TRANSFERENCIA = 'TRANSFERENCIA',
    PAGOMOVIL = 'PAGOMOVIL'
}

export enum TransactionStatus {
    CREADA = 'CREADA',
    OBSERVADA = 'OBSERVADA',
    PROCESANDO = 'PROCESANDO',
    PAGADA = 'PAGADA'
}