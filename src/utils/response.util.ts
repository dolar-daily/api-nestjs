export class ResponseUtil<T> {
  data: T | null;
  message: string;
  success: boolean;

  constructor(data: T | null, message: string, success: boolean) {
    this.data = data;
    this.message = message;
    this.success = success;
  }

  static success<T>(data: T | null, message: string): ResponseUtil<T> {
    return new ResponseUtil(data, message, true);
  }

  static error<T>(message: string, data: T | null = null): ResponseUtil<T> {
    return new ResponseUtil(data, message, false);
  }
}
