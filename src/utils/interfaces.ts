import { Request } from "express";

export interface RequestWithUser extends Request {
    user: {
        id: number;
        name: string
        typeUser: string
        sucursalId: number
        integratorId: number
        role: string
        system: string
    }
}