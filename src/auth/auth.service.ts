import { HttpException, Injectable } from "@nestjs/common";
import { LoginAuthDto } from "./dto/login-auth.dto";
import { Repository } from "typeorm";
import { User } from "src/users/entities/user.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { compare } from "bcrypt";
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,

    private jwtAuthService: JwtService
  ) {}

  async login(loginAuthDto: LoginAuthDto) {
    const { user, password } = loginAuthDto;
    const findUser = await this.userRepository.findOne({
      where: { user },
      relations: ["sucursal", "integrator"],
    });
    if (!findUser) throw new HttpException("USER_NOT_FOUND", 404);

    const checkPassword = await compare(password, findUser.password);
    if (!checkPassword) throw new HttpException("PASSWORD_ERROR", 403);

    const playload = {
      id: findUser.id,
      name: findUser.name,
      sucursalId: findUser.sucursal ? findUser.sucursal.id : null,
      integratorId: findUser.integrator ? findUser.integrator.id : null,
      role: findUser.role,
      system: "ApiRemesas",
    };

    const token = this.jwtAuthService.sign(playload);

    const data = {
      user: { ...findUser, password: null },
      token: token,
    };

    return data;
  }
}
