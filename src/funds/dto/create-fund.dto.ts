import { ApiProperty } from "@nestjs/swagger"
import { IsNumber, IsNotEmpty } from "class-validator"


export class CreateFundDto {
    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    monto: number

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    integrator: number

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    country: number
}
