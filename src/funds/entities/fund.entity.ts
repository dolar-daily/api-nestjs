import { ApiProperty } from "@nestjs/swagger";
import { Country } from "src/countries/entities/country.entity";
import { Integrator } from "src/integrators/entities/integrator.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Fund {
    @ApiProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty()
    @Column({ type: "decimal", precision: 10, scale: 2, default: 0.00 })
    amount: number;

    @ApiProperty()
    @ManyToOne(() => Integrator)
    integrator: Integrator;

    @ApiProperty()
    @ManyToOne(() => Country)
    country: Country;

    @ApiProperty()
    @DeleteDateColumn()
    deletedAt: Date;

    @ApiProperty()
    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @ApiProperty()
    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;

}
