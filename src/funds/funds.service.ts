import { Injectable } from '@nestjs/common';
import { CreateFundDto } from './dto/create-fund.dto';
import { UpdateFundDto } from './dto/update-fund.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Fund } from './entities/fund.entity';
import { Repository } from 'typeorm';
import { ResponseUtil } from 'src/utils/response.util';

@Injectable()
export class FundsService {

  constructor(
    @InjectRepository(Fund)
    private readonly fundsRepository: Repository<Fund>
  ){}
  create(createFundDto: CreateFundDto) {
    return 'This action adds a new fund';
  }

  async findAll(user) {
    let consulta = this.fundsRepository.createQueryBuilder("funds");
    if(user.role == 'SINTEGRADOR')
    {
      consulta = consulta.where("integratorId = :integratorId", { integratorId: user.integratorId });
    }
    consulta = consulta.leftJoinAndSelect("funds.country", "country");
    const resultado = await consulta.getMany();

    return ResponseUtil.success(resultado, "Sus fondos disponibles son los siguientes")
  }
  
  findOne(id: number) {
    return `This action returns a #${id} fund`;
  }

  update(id: number, updateFundDto: UpdateFundDto) {
    return `This action updates a #${id} fund`;
  }

  remove(id: number) {
    return `This action removes a #${id} fund`;
  }
}
