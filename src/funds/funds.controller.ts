import { Controller, Get, Post, Body, Patch, Param, Delete, Request, UseGuards } from '@nestjs/common';
import { FundsService } from './funds.service';
import { CreateFundDto } from './dto/create-fund.dto';
import { UpdateFundDto } from './dto/update-fund.dto';
import { ApiBearerAuth, ApiTags, ApiBasicAuth } from '@nestjs/swagger';
import { AuthGuard } from 'src/auth/auth.guard';
import { Roles } from 'src/decorators/roles.decorator';
import { RolesGuard } from 'src/auth/roles/roles.guard';

@ApiBearerAuth()
@ApiTags("Fondos")
@UseGuards(AuthGuard, RolesGuard)
@ApiBasicAuth("Bearer")
@Controller('funds')
export class FundsController {
  constructor(private readonly fundsService: FundsService) {}

  @Roles('SADMIN')
  @Post()
  create(@Body() createFundDto: CreateFundDto) {
    return this.fundsService.create(createFundDto);
  }

  @Roles(['SADMIN', 'SINTEGRADOR'])
  @Get()
  findAll(@Request() req) {
    return this.fundsService.findAll(req.user);
  }

  @Roles('SADMIN')
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.fundsService.findOne(+id);
  }

  @Roles('SADMIN')
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateFundDto: UpdateFundDto) {
    return this.fundsService.update(+id, updateFundDto);
  }

  @Roles('SADMIN')
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.fundsService.remove(+id);
  }
}
