import { Module } from '@nestjs/common';
import { FundsService } from './funds.service';
import { FundsController } from './funds.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Fund } from './entities/fund.entity';
import { Country } from 'src/countries/entities/country.entity';
import { Integrator } from 'src/integrators/entities/integrator.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Fund, Country, Integrator])],
  controllers: [FundsController],
  providers: [FundsService],
})
export class FundsModule {}
