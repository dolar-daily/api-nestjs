import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { hash } from "bcrypt";
import { User } from "src/users/entities/user.entity";
import { Repository } from "typeorm";

@Injectable()
export class SeederService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ) {}

  async seed() {
    const findUser = await this.userRepository.findOne({
      where: { user: "sadmin" },
    });
    if (!findUser) {
      const plain = "sadmin";
      const plainToHash = await hash(plain, 10);
      const usersToSeed = [
        {
          name: "Super Admin",
          user: "sadmin",
          password: plainToHash,
          role: "SADMIN",
        },
      ];

      await Promise.all(
        usersToSeed.map(async (userData) => {
          const user = this.userRepository.create(userData);
          await this.userRepository.save(user);
        })
      );
    }

    const findIntegrator = await this.userRepository.findOne({
      where: { user: "integrator" },
    });
    if (!findIntegrator) {
      const plain = "integrator";
      const plainToHash = await hash(plain, 10);
      const usersToSeed = [
        {
          name: "Integraor Demo",
          user: "integrator",
          password: plainToHash,
          role: "SINTEGRADOR",
        },
      ];

      await Promise.all(
        usersToSeed.map(async (userData) => {
          const user = this.userRepository.create(userData);
          await this.userRepository.save(user);
        })
      );
    }
  }
}
