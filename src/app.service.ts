import { Injectable } from "@nestjs/common";
import { json } from "stream/consumers";

@Injectable()
export class AppService {
  getHello() {
    return {
      message: "Api Dolar Daily",
      version: 0.1,
      meta: {
        developer: "Armando Campos",
        web: "acamposdigital.com",
      },
    };
  }
}
