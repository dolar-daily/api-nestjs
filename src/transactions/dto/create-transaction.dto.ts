import { ApiProperty } from "@nestjs/swagger"
import { IsNumber, IsNotEmpty, ValidateIf, IsOptional, IsString } from "class-validator"
import { Integrator } from "src/integrators/entities/integrator.entity"
import { TransactionType } from "src/utils/enums"

export class CreateTransactionDto {
    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    client: number

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    rate: number

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    country: number

    @IsNotEmpty()
    typeTransaction: TransactionType;

    @ValidateIf(o => o.typeTransaction === TransactionType.TRANSFERENCIA)
    @IsNotEmpty()
    account_number: number;

    @ValidateIf(o => o.typeTransaction === TransactionType.PAGOMOVIL)
    @IsNotEmpty()
    numero_telefono: number;

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    bank: number

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    titular: string

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    documento: string

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    monto_recibido: number
}