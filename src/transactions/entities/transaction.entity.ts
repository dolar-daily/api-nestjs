import { ApiProperty } from "@nestjs/swagger";
import { BanksCountry } from "src/banks-countries/entities/banks-country.entity";
import { Client } from "src/clients/entities/client.entity";
import { Country } from "src/countries/entities/country.entity";
import { Integrator } from "src/integrators/entities/integrator.entity";
import { Rate } from "src/rates/entities/rate.entity";
import { User } from "src/users/entities/user.entity";
import { TransactionStatus, TransactionType } from "src/utils/enums";
import { CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn, Column } from "typeorm";

@Entity()
export class Transaction {
   
    @ApiProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => User)
    user: User //listo

    @ManyToOne(() => Integrator)
    integrator: Integrator //listo

    @ManyToOne(() => Client)
    client: Client //listo

    @ApiProperty({ enum: TransactionType })
    @Column({ type: 'enum', enum: TransactionType })
    typeTransaction: TransactionType; //listo

    // transferencia y pago movil
    @ManyToOne(() => BanksCountry, {nullable: true})
    bank: BanksCountry //listo

    // transferencia y pago movil
    @Column({nullable: true})
    titular: string

    // transferencia y pago movil
    @Column({nullable: true})
    documento: string

    // transferencia
    @Column({nullable: true})
    account_number: string //listo

    // pago movil
    @Column({nullable: true})
    numero_telefono: string //listo

    @Column({ type: "decimal", precision: 10, scale: 2, default: 0.00 })
    monto_recibido: number;

    @Column({ type: "decimal", precision: 10, scale: 2, default: 0.00 })
    monto_destino: number;

    @Column()
    moneda_recibir: string;

    @Column({nullable: true})
    codigo_transaccion: number;

    @Column({nullable: true})
    comprobante_transaccion: string;

    @Column()
    pais_destino: string;

    @Column({ type: "decimal", precision: 10, scale: 2, default: 0.00 })
    tasa_calculada: number;

    @ManyToOne(() => Rate)
    rate: Rate; //listo

    @ManyToOne(() => Country)
    country: Country; //listo

    @ApiProperty({ enum: TransactionStatus })
    @Column({ type: 'enum', enum: TransactionStatus })
    status: TransactionStatus;

    @ApiProperty()
    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @ApiProperty()
    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
    
}
