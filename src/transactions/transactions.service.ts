import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BanksCountry } from 'src/banks-countries/entities/banks-country.entity';
import { Client } from 'src/clients/entities/client.entity';
import { Country } from 'src/countries/entities/country.entity';
import { Rate } from 'src/rates/entities/rate.entity';
import { ResponseUtil } from 'src/utils/response.util';
import { Repository } from 'typeorm';
import { Transaction } from './entities/transaction.entity';
import { User } from 'src/users/entities/user.entity';
import { Integrator } from 'src/integrators/entities/integrator.entity';
import { TransactionStatus } from 'src/utils/enums';
import { Fund } from 'src/funds/entities/fund.entity';

@Injectable()
export class TransactionsService {

  constructor(
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
    @InjectRepository(Rate)
    private readonly rateRepository: Repository<Rate>,
    @InjectRepository(Country)
    private readonly countryRepository: Repository<Country>,
    @InjectRepository(BanksCountry)
    private readonly bankRepository: Repository<BanksCountry>,
    @InjectRepository(Transaction)
    private readonly transactionRepository: Repository<Transaction>,
    @InjectRepository(Fund)
    private readonly fundsRepository: Repository<Fund>
  ){}

  async create(createTransactionDto, user) {
    // validar cliente
    const vClient = await this.clientRepository.findOneBy({id: createTransactionDto.client})
    if(vClient === null)
    {
      return ResponseUtil.error("Este cliente no esta reistrado, registrelo e intente nuevamente")
    }
    // validar tasa
    const vRate = await this.rateRepository.findOneBy({id: createTransactionDto.rate})
    if(vRate === null)
    {
      return ResponseUtil.error("La tasa declarada no existe, intente nuevamente con una correcta")
    }
    // validar pais
    const vCountry = await this.countryRepository.findOneBy({id: createTransactionDto.country})
    if(vCountry === null)
    {
      return ResponseUtil.error("El pais que intentas consultar no existe intenta nuevamente")
    }
    // validar banco
    const vBank = await this.bankRepository.findOneBy({id: createTransactionDto.bank})
    if(vBank === null)
    {
      return ResponseUtil.error("El banco que intentas consultar no existe, intenta de nuevo")
    }

    let cuenta: null
    let numero: null

    if(createTransactionDto.typeTransaction === 'TRANSFERENCIA')
    {
      cuenta = createTransactionDto.account_number
    }
    else{
      numero = createTransactionDto.numero_telefono
    }

    let monto_destino = createTransactionDto.monto_recibido * vRate.monto

    const vUser = new User()
    vUser.id = user.id

    const vIntegrator = new Integrator()
    vIntegrator.id = user.integratorId

    const playload = {
      typeTransaction: createTransactionDto.typeTransaction,
      titular: createTransactionDto.titular,
      documento: createTransactionDto.documento,
      account_number: cuenta,
      numero_telefono: numero,
      status: TransactionStatus.CREADA,
      client: vClient,
      bank: vBank,
      rate: vRate,
      country: vCountry,
      user: vUser,
      integrator: vIntegrator,
      monto_recibido: createTransactionDto.monto_recibido,
      monto_destino: monto_destino,
      moneda_recibir: vCountry.currency,
      pais_destino: vCountry.name,
      tasa_calculada: vRate.monto
    }

    const vFunds = await this.fundsRepository.createQueryBuilder("funds")
    .where("countryId = :countryId", { countryId: vCountry.id })
    .andWhere("integratorId = :integratorId", { integratorId: vIntegrator.id })
    .getOne();

    if(vFunds === null)
    {
      return ResponseUtil.error("No estas autorizado para realizar transacciones hacia este pais, contacta con Soporte Tecnico")
    }

    if(vFunds.amount >= monto_destino)
    {
        let newMonto = vFunds.amount - monto_destino
        await this.fundsRepository.update(vFunds.id, {amount: newMonto})
    }
    else{
      return ResponseUtil.error("No cuentas con saldo suficiente para poder realizar esta transaccion, consulta con administracion")
    }

    const data = await this.transactionRepository.save(playload)
    return ResponseUtil.success(data, "Transaccion Creada con exito")
  }

  async findAll(user) {
    let consulta = this.transactionRepository.createQueryBuilder("transactions");

    if(user.role == 'SINTEGRADOR')
    {
      consulta = consulta.where("integratorId = :integratorId", { integratorId: user.integratorId });
    }

    const resultado = await consulta.getMany();

    return ResponseUtil.success(resultado, "Todas tus transacciones")
  }

  async findOne(id: number) {
    const tranbsaccion = await this.transactionRepository.findOne({ 
      where: { id },
      relations: ["client", "bank", "country"]
    });

    if(tranbsaccion === null)
    {
      return ResponseUtil.error("Esta transaccion no existe")
    }

    return ResponseUtil.success(tranbsaccion, "Transaccion encontrada con exito")
  }

  update(id: number, updateTransactionDto) {
    return `This action updates a #${id} transaction`;
  }

  remove(id: number) {
    return `This action removes a #${id} transaction`;
  }
}
