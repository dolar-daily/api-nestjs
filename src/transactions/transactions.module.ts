import { Module } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Transaction } from './entities/transaction.entity';
import { Client } from 'src/clients/entities/client.entity';
import { Rate } from 'src/rates/entities/rate.entity';
import { BanksCountry } from 'src/banks-countries/entities/banks-country.entity';
import { Country } from 'src/countries/entities/country.entity';
import { Fund } from 'src/funds/entities/fund.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Transaction, Client, Rate, BanksCountry, Country, Fund])],
  controllers: [TransactionsController],
  providers: [TransactionsService],
})
export class TransactionsModule {}
