import { ApiProperty } from "@nestjs/swagger";
import { Integrator } from "src/integrators/entities/integrator.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Client {
    @ApiProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty()
    @Column()
    names: string

    @ApiProperty()
    @Column()
    phone: string

    @ApiProperty()
    @Column()
    document: number

    @ApiProperty()
    @Column()
    typeDocument: string

    @ManyToOne(() => Integrator, { nullable: true })
    integrator: Integrator

    @OneToMany(() => Transaction, transaction => transaction.client)
    transactions: Transaction[];

    @ApiProperty()
    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @ApiProperty()
    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
}
