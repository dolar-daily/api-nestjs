import { Injectable } from '@nestjs/common';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Client } from './entities/client.entity';
import { Repository } from 'typeorm';
import { ResponseUtil } from 'src/utils/response.util';
import { Integrator } from 'src/integrators/entities/integrator.entity';

@Injectable()
export class ClientsService {

  constructor(
    @InjectRepository(Client)
    private readonly clientsRepository: Repository<Client>,
    @InjectRepository(Integrator)
    private readonly integratorRepository: Repository<Integrator>
  ){}
  
  async create(createClientDto: CreateClientDto, user) {
    const validate = await this.clientsRepository.findOneBy({document: createClientDto.document})

    if(validate !== null)
    {
      return ResponseUtil.error("Cliente con este documento ya existe")
    }

    if(user.role == 'SINTEGRADOR'){
      const integrator = await this.integratorRepository.findOneBy({id: user.integratorId})
      createClientDto.integrator = integrator;
    }

    const data = await this.clientsRepository.save(createClientDto)
    return ResponseUtil.success(data, "Cliente Registrado con exito")
  }         

  async findAll(user) {
    let consulta = await this.clientsRepository.createQueryBuilder("clients");
    if(user.role == 'SINTEGRADOR')
    {
      consulta = consulta.where("integratorId = :integratorId", { integratorId: user.integratorId });
    }

    const resultado = await consulta.getMany();

    return ResponseUtil.success(resultado, "Este es su listado de clientes")
  }

  async findOne(document: number) {
    const client = await this.clientsRepository.findOne({ 
      where: { document: document },
      relations: ["transactions"]
    });
    
    if(client === null)
    {
      return ResponseUtil.error("Este numero de documento no existe en nuestra base de datos.")
    }
    
    return ResponseUtil.success(client, "Visualizas el cliete y su historial")

  }

  update(id: number, updateClientDto: UpdateClientDto) {
    return `This action updates a #${id} client`;
  }

  remove(id: number) {
    return `This action removes a #${id} client`;
  }
}
