import { ApiProperty } from "@nestjs/swagger"
import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator"
import { Integrator } from "src/integrators/entities/integrator.entity"

export class CreateClientDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    names: string

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    phone: string

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    document: number

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    typeDocument: string

    @ApiProperty()
    @IsString()
    @IsOptional()
    integrator: Integrator
}
