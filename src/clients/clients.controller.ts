import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Request } from '@nestjs/common';
import { ClientsService } from './clients.service';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';
import { ApiBearerAuth, ApiTags, ApiBasicAuth } from '@nestjs/swagger';
import { AuthGuard } from 'src/auth/auth.guard';
import { RolesGuard } from 'src/auth/roles/roles.guard';
import { Roles } from 'src/decorators/roles.decorator';

@ApiBearerAuth()
@ApiTags("Clientes")
@UseGuards(AuthGuard, RolesGuard)
@ApiBasicAuth("Bearer")
@Controller('clients')
export class ClientsController {
  constructor(private readonly clientsService: ClientsService) {}

  @Roles(['SADMIN', 'SINTEGRADOR'])
  @Post()
  create(@Body() createClientDto: CreateClientDto, @Request() req) {
    return this.clientsService.create(createClientDto, req.user);
  }

  @Roles(['SADMIN', 'SINTEGRADOR'])
  @Get()
  findAll(@Request() req) {
    return this.clientsService.findAll(req.user);
  }

  @Roles(['SADMIN', 'SINTEGRADOR'])
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.clientsService.findOne(+id);
  }

  @Roles(['SADMIN', 'SINTEGRADOR'])
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateClientDto: UpdateClientDto) {
    return this.clientsService.update(+id, updateClientDto);
  }

  @Roles(['SADMIN'])
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.clientsService.remove(+id);
  }
}
