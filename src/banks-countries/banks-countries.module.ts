import { Module } from '@nestjs/common';
import { BanksCountriesService } from './banks-countries.service';
import { BanksCountriesController } from './banks-countries.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BanksCountry } from './entities/banks-country.entity';
import { Country } from 'src/countries/entities/country.entity';

@Module({
  imports: [TypeOrmModule.forFeature([BanksCountry, Country])],
  controllers: [BanksCountriesController],
  providers: [BanksCountriesService],
})
export class BanksCountriesModule {}
