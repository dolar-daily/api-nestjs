import { ApiProperty } from "@nestjs/swagger";
import { Country } from "src/countries/entities/country.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class BanksCountry {
    @ApiProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string

    @Column()
    name: string

    @Column({ default: true })
    status: boolean

    @ManyToOne(() => Country, country => country.banks)
    country: Country;

    @ApiProperty()
    @DeleteDateColumn()
    deletedAt: Date;

    @ApiProperty()
    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @ApiProperty()
    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
}
