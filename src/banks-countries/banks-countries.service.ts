import { Injectable } from '@nestjs/common';
import { CreateBanksCountryDto } from './dto/create-banks-country.dto';
import { UpdateBanksCountryDto } from './dto/update-banks-country.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { BanksCountry } from './entities/banks-country.entity';
import { Repository } from 'typeorm';
import { Country } from 'src/countries/entities/country.entity';
import { ResponseUtil } from 'src/utils/response.util';

@Injectable()
export class BanksCountriesService {

  constructor(
    @InjectRepository(Country)
    private readonly countriesRepository: Repository<Country>,
    @InjectRepository(BanksCountry)
    private readonly banksCountryRepository: Repository<BanksCountry>
  ){}

  async create(createBanksCountryDto: CreateBanksCountryDto) {
    const id = createBanksCountryDto.country
    const validate = await this.countriesRepository.findOneBy({id})
    if(validate == null)
    {
      return ResponseUtil.error('No se encontro ese pais, intente nuevamente validando la lista de paises disponibles')
    }

    const duplicate = this.banksCountryRepository.findOneBy({code: createBanksCountryDto.code})

    if(duplicate !== null)
    {
      return ResponseUtil.error('Ya existe un banco con ese codigo.')
    }
    const register = this.banksCountryRepository.save({
      name: createBanksCountryDto.name, 
      code: createBanksCountryDto.code,
      country: validate
    })
   
    return ResponseUtil.success(register, 'Registrado con exito')
  }

  async findAll() {
    const banks = await this.banksCountryRepository.find()

    return ResponseUtil.success(banks, 'Listado de registros obtenidos correctamente')
  }

  async findOne(id: number) {
    console.log(id)
    const data = await this.countriesRepository.createQueryBuilder("country")
    .leftJoinAndSelect("country.banks", "banks")
    .where("country.id = :id", { id: id })
    .getOne();

    if(data === null)
    {
        return ResponseUtil.error('Este Banco no existe')
    }

    return ResponseUtil.success(data, 'Registro encontrado con exito')
  }

  async update(id: number, updateBanksCountryDto: UpdateBanksCountryDto) {
    const validate = await this.banksCountryRepository.findOneBy({id})
    if(validate === null)
    {
      return ResponseUtil.error('Este Banco no existe')
    }

    const update = await this.banksCountryRepository.update(id, {name: updateBanksCountryDto.name, code: updateBanksCountryDto.code})

    return ResponseUtil.success(update, 'Registro editado con exito')
  }

  remove(id: number) {
    return `This action removes a #${id} banksCountry`;
  }
}
