import { PartialType } from '@nestjs/swagger';
import { CreateBanksCountryDto } from './create-banks-country.dto';

export class UpdateBanksCountryDto extends PartialType(CreateBanksCountryDto) {}
