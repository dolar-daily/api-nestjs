import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { BanksCountriesService } from './banks-countries.service';
import { CreateBanksCountryDto } from './dto/create-banks-country.dto';
import { UpdateBanksCountryDto } from './dto/update-banks-country.dto';
import { ApiBearerAuth, ApiTags, ApiBasicAuth } from '@nestjs/swagger';
import { AuthGuard } from 'src/auth/auth.guard';
import { Roles } from 'src/decorators/roles.decorator';
import { RolesGuard } from 'src/auth/roles/roles.guard';

@ApiBearerAuth()
@ApiTags("Bancos por Pais")
@UseGuards(AuthGuard, RolesGuard)
@ApiBasicAuth("Bearer")
@Controller('banks')
export class BanksCountriesController {
  constructor(private readonly banksCountriesService: BanksCountriesService) {}

  @Roles('SADMIN')
  @Post()
  create(@Body() createBanksCountryDto: CreateBanksCountryDto) {
    return this.banksCountriesService.create(createBanksCountryDto);
  }

  @Roles(['SADMIN'])
  @Get()
  findAll() {
    return this.banksCountriesService.findAll();
  }

  @Roles(['SADMIN', 'SINTEGRADOR'])
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.banksCountriesService.findOne(+id);
  }

  @Roles(['SADMIN'])
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBanksCountryDto: UpdateBanksCountryDto) {
    return this.banksCountriesService.update(+id, updateBanksCountryDto);
  }

  @Roles(['SADMIN'])
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.banksCountriesService.remove(+id);
  }
}
