import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from "@nestjs/common";
import { CountriesService } from "./countries.service";
import { CreateCountryDto } from "./dto/create-country.dto";
import { UpdateCountryDto } from "./dto/update-country.dto";
import { AuthGuard } from "src/auth/auth.guard";
import {
  ApiBasicAuth,
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { Country } from "./entities/country.entity";
import { RolesGuard } from "src/auth/roles/roles.guard";
import { Roles } from "src/decorators/roles.decorator";

@ApiBearerAuth()
@ApiTags("Paises")
@UseGuards(AuthGuard, RolesGuard)
@ApiBasicAuth("Bearer")
@Controller("countries")
export class CountriesController {
  constructor(private readonly countriesService: CountriesService) {}

  @ApiOperation({ summary: "Crear un nuevo país" })
  @ApiResponse({ status: 201, type: Country })
  @Roles(['SADMIN'])
  @Post()
  @ApiOperation({ summary: "Crear Pais" })
  create(@Body() createCountryDto: CreateCountryDto) {
    return this.countriesService.create(createCountryDto);
  }

  @ApiOperation({ summary: "Obtener todos los países" })
  @ApiResponse({ status: 200, type: [Country] })
  @Roles(['SADMIN'])
  @Get()
  findAll() {
    return this.countriesService.findAll();
  }

  @ApiOperation({ summary: "Obtener un país por ID" })
  @ApiResponse({ status: 200, type: Country })
  @ApiResponse({ status: 404, description: "País no encontrado" })
  @Roles(['SADMIN'])
  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.countriesService.findOne(+id);
  }

  @ApiOperation({ summary: "Actualizar un país" })
  @ApiResponse({ status: 200, type: Country })
  @ApiResponse({ status: 404, description: "País no encontrado" })
  @Roles(['SADMIN'])
  @Patch(":id")
  update(@Param("id") id: string, @Body() updateCountryDto: UpdateCountryDto) {
    return this.countriesService.update(+id, updateCountryDto);
  }

  @ApiOperation({ summary: "Eliminar un país" })
  @ApiResponse({ status: 200, description: "País eliminado correctamente" })
  @ApiResponse({ status: 404, description: "País no encontrado" })
  @Roles(['SADMIN'])
  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.countriesService.remove(+id);
  }
}
