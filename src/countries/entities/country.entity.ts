import { ApiProperty } from "@nestjs/swagger";
import { BanksCountry } from "src/banks-countries/entities/banks-country.entity";
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Country {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  name: string;

  @ApiProperty()
  @Column()
  abbreviation: string;

  @ApiProperty()
  @Column()
  currency: string;

  @OneToMany(() => BanksCountry, bank => bank.country)
  banks: BanksCountry[];

  @ApiProperty()
  @DeleteDateColumn()
  deletedAt: Date;

  @ApiProperty()
  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @ApiProperty()
  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
