import { Injectable } from "@nestjs/common";
import { CreateCountryDto } from "./dto/create-country.dto";
import { UpdateCountryDto } from "./dto/update-country.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Country } from "./entities/country.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
@Injectable()
export class CountriesService {
  constructor(
    @InjectRepository(Country)
    private readonly countriesRepository: Repository<Country>
  ) {}

  async create(createCountryDto: CreateCountryDto) {
    const findCountry = await this.countriesRepository.findOneBy({
      name: createCountryDto.name,
    });
    console.log(findCountry);
    if (findCountry) {
      return ResponseUtil.error(
        "Pais con nombre: " +
          createCountryDto.name +
          " ya existe, intente con otro nombre"
      );
    }

    const data = await this.countriesRepository.save(createCountryDto);
    return ResponseUtil.success(data, "Pais creado exitosamente");
  }

  async findAll() {
    const data = await this.countriesRepository.find();
    return ResponseUtil.success(data, "Datos obtenidos correctamente");
  }

  async findOne(id: number) {
    const data = await this.countriesRepository.findOneBy({ id });

    if (!data) {
      return ResponseUtil.error("Registro no encontrado");
    }

    return ResponseUtil.success(data, "Registro encontrado con exito");
  }

  async update(id: number, updateCountryDto: UpdateCountryDto) {
    const country = await this.countriesRepository.findOneBy({ id });

    if (!country) {
      return ResponseUtil.error("Registro no existe");
    }

    await this.countriesRepository.update(id, updateCountryDto);

    const data = await this.countriesRepository.findOneBy({ id });
    return ResponseUtil.success(data, "Registro Actualizado con exito");
  }

  async remove(id: number) {
    const country = await this.countriesRepository.findOneBy({ id });

    if (!country) {
      return ResponseUtil.error("Registro no existe");
    }

    await this.countriesRepository.softRemove(country);

    return ResponseUtil.success(country, "Registro Eliminado con exito");
  }
}
