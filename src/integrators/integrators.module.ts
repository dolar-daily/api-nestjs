import { Module } from '@nestjs/common';
import { IntegratorsService } from './integrators.service';
import { IntegratorsController } from './integrators.controller';

@Module({
  controllers: [IntegratorsController],
  providers: [IntegratorsService],
})
export class IntegratorsModule {}
