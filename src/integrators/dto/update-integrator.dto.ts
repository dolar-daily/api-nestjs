import { PartialType } from '@nestjs/swagger';
import { CreateIntegratorDto } from './create-integrator.dto';

export class UpdateIntegratorDto extends PartialType(CreateIntegratorDto) {}
