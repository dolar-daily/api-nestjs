import { Injectable } from '@nestjs/common';
import { CreateIntegratorDto } from './dto/create-integrator.dto';
import { UpdateIntegratorDto } from './dto/update-integrator.dto';

@Injectable()
export class IntegratorsService {
  create(createIntegratorDto: CreateIntegratorDto) {
    return 'This action adds a new integrator';
  }

  findAll() {
    return `This action returns all integrators`;
  }

  findOne(id: number) {
    return `This action returns a #${id} integrator`;
  }

  update(id: number, updateIntegratorDto: UpdateIntegratorDto) {
    return `This action updates a #${id} integrator`;
  }

  remove(id: number) {
    return `This action removes a #${id} integrator`;
  }
}
