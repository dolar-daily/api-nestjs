import { SucursalsIntegrator } from "src/sucursals_integrators/entities/sucursals_integrator.entity";
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Integrator {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  document: string;

  @Column()
  phone: string;

  @Column()
  address: string;

  @Column()
  city: string;

  @Column()
  email: string;

  @Column({ default: false })
  companty: boolean;

  @OneToMany(() => SucursalsIntegrator, (account) => account.integrator)
  sucursales: SucursalsIntegrator[];

  @DeleteDateColumn()
  deletedAt: Date;

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
