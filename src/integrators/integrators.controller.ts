import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { IntegratorsService } from './integrators.service';
import { CreateIntegratorDto } from './dto/create-integrator.dto';
import { UpdateIntegratorDto } from './dto/update-integrator.dto';
import { ApiBasicAuth, ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from 'src/auth/auth.guard';
import { RolesGuard } from 'src/auth/roles/roles.guard';
import { Roles } from 'src/decorators/roles.decorator';

@ApiBearerAuth()
@ApiTags("Integradores")
@UseGuards(AuthGuard, RolesGuard)
@ApiBasicAuth("Bearer")
@Controller('integrators')
export class IntegratorsController {
  constructor(private readonly integratorsService: IntegratorsService) {}

  @Roles(['SADMIN'])
  @Post()
  create(@Body() createIntegratorDto: CreateIntegratorDto) {
    return this.integratorsService.create(createIntegratorDto);
  }

  @Roles(['SADMIN'])
  @Get()
  findAll() {
    return this.integratorsService.findAll();
  }

  @Roles(['SADMIN'])
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.integratorsService.findOne(+id);
  }

  @Roles(['SADMIN'])
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateIntegratorDto: UpdateIntegratorDto) {
    return this.integratorsService.update(+id, updateIntegratorDto);
  }

  @Roles(['SADMIN'])
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.integratorsService.remove(+id);
  }
}
