import { Module } from "@nestjs/common";
import { UsersService } from "./users.service";
import { UsersController } from "./users.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "./entities/user.entity";
import { Integrator } from "src/integrators/entities/integrator.entity";
import { SucursalsIntegrator } from "src/sucursals_integrators/entities/sucursals_integrator.entity";

@Module({
  imports: [TypeOrmModule.forFeature([User, Integrator, SucursalsIntegrator])],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
