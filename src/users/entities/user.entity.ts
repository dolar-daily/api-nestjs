import { Integrator } from "src/integrators/entities/integrator.entity";
import { SucursalsIntegrator } from "src/sucursals_integrators/entities/sucursals_integrator.entity";
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  user: string;

  @Column()
  password: string;

  @Column({ default: "user" })
  role: string;

  @ManyToOne(() => Integrator, { nullable: true })
  integrator: Integrator;

  @ManyToOne(() => SucursalsIntegrator, { nullable: true })
  sucursal: SucursalsIntegrator;

  @DeleteDateColumn()
  deletedAt: Date;

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
